/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication15;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author estudiantes
 */
public class JavaApplication15 extends JFrame {

    /**
     * @param args the command line arguments
     */
    public JavaApplication15() {
        setSize(300, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JTextArea textArea = new JTextArea();
        textArea.setText("Click Me!");

        textArea.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.NOBUTTON) {
                    textArea.setText("No button clicked...");
                } else if (e.getButton() == MouseEvent.BUTTON1) {
                    textArea.setText("Button 1 clicked...");
                } else if (e.getButton() == MouseEvent.BUTTON2) {
                    textArea.setText("Button 2 clicked...");
                } else if (e.getButton() == MouseEvent.BUTTON3) {
                    textArea.setText("Button 3 clicked...");
                }

                System.out.println("Number of click: " + e.getClickCount());
                System.out.println("Click position (X, Y):  " + e.getX() + ", " + e.getY());
            }
        });

        getContentPane().add(textArea);
    }

    public static void main(String[] args) {
        new JavaApplication15().setVisible(true);
    }

}
