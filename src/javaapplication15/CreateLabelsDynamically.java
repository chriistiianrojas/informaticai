/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication15;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import javax.swing.event.*;

class CreateLabelsDynamically extends JFrame implements CaretListener {

    JTextField jtf;
    JPanel jpan;
    JLabel[] jlabels;

    public CreateLabelsDynamically() {

        jtf = new JTextField(10);
        jpan = new JPanel();

        setLayout(null);

        jtf.setBounds(50, 20, 50, 30);
        jtf.addCaretListener(this);

        jpan.setBounds(50, 80, 200, 200);
        jpan.setOpaque(true);
        jpan.setBackground(Color.CYAN);

        add(jtf);
        add(jpan);
  
    }

    public void caretUpdate(CaretEvent ce) {

        String str = jtf.getText();

        jpan.removeAll();

        if (!str.equals("")) {
            int num = Integer.parseInt(str);
            jlabels = new JLabel[num];

            for (int i = 0; i < jlabels.length; i++) {
                jlabels[i] = label(String.valueOf((i + 1) + "5PRUEBAAA"));
                jpan.add(jlabels[i]);
            }
        }
        jpan.validate();
        jpan.repaint();
    }

    public static void main(String args[]) {

        CreateLabelsDynamically c = new CreateLabelsDynamically();
        c.setTitle("My Frame");
        c.setSize(300, 350);
        c.setVisible(true);
    }

    // JLabel is declared here:
    private JLabel label(String x) {
        final JLabel l;
        l = new JLabel(x);
        l.setFont(new java.awt.Font("Tahoma", 0, 16));
        l.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        l.setOpaque(true);
        l.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        l.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        l.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {

                System.out.println(l.getText());
            }
        });
        return l;
    }

}
