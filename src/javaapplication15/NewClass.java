/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication15;

import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author estudiantes
 */
public class NewClass {

    public static void main(String args[]) {
        byte[] file = getByteArrayFileContent("prueba.txt");
        System.out.println("file: " + file);
        for (int i = 0; i < file.length; i++) {
            byte b = file[i];
            String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
            System.out.println("b: " + b + " HEX: " + Integer.toHexString(b) + " ascii: " + convertHexToString);
        }

    }

    public static byte[] getByteArrayFileContent(String path) {
        byte[] content = null;
        try {
            File f = new File(path);
            FileInputStream src_file = new FileInputStream(f);
            content = new byte[(int) f.length()];
            src_file.read(content);
            src_file.close();
        } catch (Exception e) {
            System.out.println("");
        }
        return content;

    }

    public static String convertHexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        System.out.println("Decimal : " + temp.toString());

        return sb.toString();
    }
}
