/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication15;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
 
public class MouseExercise1 extends Applet implements MouseListener {
 
 int keyCode;
 
 public void init() {
  
  addMouseListener(this);
 }
 
 public void paint(Graphics g) {
  
  if(keyCode == 1) {
   
   g.drawRect(10, 10, 80, 80);
   g.drawString("Mouse left key pressed", 10, 140);
  }
  
  if(keyCode == 2) {
   
   g.drawOval(10, 10, 80, 80);
   g.drawString("Mouse right key pressed", 10, 140);
  }
 }
 
 public void mouseClicked(MouseEvent me) {
  
  if(me.getButton() == MouseEvent.BUTTON1) {
   // Left key clicked
   keyCode = 1;
  }
  
  if(me.getButton() == MouseEvent.BUTTON3) {
   // Right key clicked
   keyCode = 2;
  }
  
  repaint();
 }
 
 public void mouseEntered(MouseEvent me) { }
 
 public void mouseExited(MouseEvent me) { }
 
 public void mousePressed(MouseEvent me) { }
 
 public void mouseReleased(MouseEvent me) { }
}
 
/*  <applet code="MouseExercise1.class" height="300" width="300">
 </applet>
*/