package presentacion;

import java.awt.Color;
import java.awt.Graphics;
import logica.Sumadora;

public class Modelo implements Runnable {

    private Sumadora miSistema;
    private VistaPrincipal ventanaPrincipal;
    private Thread hiloDibujo;
    private boolean activado;

    public void iniciar() {
        activado = true;
        getVentanaPrincipal().setSize(700, 400);
        getVentanaPrincipal().setVisible(true);
        hiloDibujo = new Thread(this);
        hiloDibujo.start();
    }

    public Sumadora getMiSistema() {
        if (miSistema == null) {
            miSistema = new Sumadora();
        }
        return miSistema;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if (ventanaPrincipal == null) {
            ventanaPrincipal = new VistaPrincipal(this);
        }
        return ventanaPrincipal;
    }

    public void sumar() {
        float n1 = 0, n2 = 0, r;
        /*try {
         n1 = new Float(getVentanaPrincipal().getTxtNumero1().getText()).floatValue();
         n2 = new Float(getVentanaPrincipal().getTxtNumero2().getText()).floatValue();
         getMiSistema().setNumero1(n1);
         getMiSistema().setNumero2(n2);
         r = getMiSistema().sumar();
         getVentanaPrincipal().getLblResultado().setText("El resultado es " + r);
         } catch (NumberFormatException ex) {
         getVentanaPrincipal().getLblResultado().setText("Debe tener datos digitados!");
         }*/

    }

    public void dibujar() {
        /*
         int n1, n2, i, x = 5;        
        
         n1 = (int)getMiSistema().getNumero1();
         n2 = (int)getMiSistema().getNumero2();
        
         Graphics lapiz = getVentanaPrincipal().getLienzo().getGraphics();
         lapiz.setColor(Color.BLUE);        
                
         for(i=0; i<n1; i++){
         lapiz.fillOval(x, 20, 10, 10);
         x += 10;
         }
        
         lapiz.setColor(Color.RED);        
         x = 5;      
         for(i=0; i<n2; i++){
         lapiz.fillOval(x, 40, 10, 10);
         x += 10;
         }*/
    }

    @Override
    public void run() {
        while (activado) {
            dibujar();
        }
    }

}
