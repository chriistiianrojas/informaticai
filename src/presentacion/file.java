/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author Christian
 */
public class file {

    public static byte[] getByteArrayFileContent(String path,File f) {
        byte[] content = null;
        try {
            //File f = new File(path);
            FileInputStream src_file = new FileInputStream(f);
            content = new byte[(int) f.length()];
            src_file.read(content);
            src_file.close();
        } catch (Exception e) {
            System.out.println("");
        }
        return content;

    }

    public static String convertHexToString(String hex) {

        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for (int i = 0; i < hex.length() - 1; i += 2) {

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        System.out.println("Decimal : " + temp.toString());

        return sb.toString();
    }

}
