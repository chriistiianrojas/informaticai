/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentacion;

import java.awt.Canvas;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static presentacion.file.getByteArrayFileContent;
import static presentacion.file.convertHexToString;

/**
 *
 * @author estudiantes
 */
public class VistaPrincipal extends javax.swing.JFrame {

    private Modelo model;
    private Controlador control;

    private final JFileChooser ofile;

    public VistaPrincipal(Modelo aThis) {
        model = aThis;
        initComponents();
        capturarEventos();
        ofile = new JFileChooser();
        ofile.setCurrentDirectory(new File("./temp"));
    }

    public Controlador getControl() {
        if (control == null) {
            control = new Controlador(this);
        }
        return control;
    }

    public Modelo getModel() {
        return model;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPrincipal = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().add(jPanelPrincipal);
        jPanelPrincipal.setBounds(100, 20, 360, 190);

        jMenu1.setText("File");

        jMenuItem1.setText("Abrir archivo");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public JPanel getjPanelPrincipal() {
        return jPanelPrincipal;
    }


    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        int resultfile = ofile.showOpenDialog(this);
        if (resultfile == JFileChooser.APPROVE_OPTION) {
            try {
                byte[] file = getByteArrayFileContent("prueba.txt", ofile.getSelectedFile());
                System.out.println("file: " + file);
                for (int i = 0; i < file.length; i++) {
                    byte b = file[i];
                    String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
                    System.out.println("b: " + b + " HEX: " + Integer.toHexString(b) + " ascii: " + convertHexToString);
                }
                //file.getByteArrayFileContent("", ofile.getSelectedFile());
            } catch (Exception e) {
            }
        } else {
            System.out.println("No se cargo el archivo");
        }

    }//GEN-LAST:event_jMenuItem1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanelPrincipal;
    // End of variables declaration//GEN-END:variables

    private void capturarEventos() {
        //btnSumar.addActionListener(getControl());
    }
}
